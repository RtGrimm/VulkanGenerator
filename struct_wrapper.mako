<%def name="comma(item, items)">\
%if not item == items[-1]:
  ,\
%endif
</%def>

struct ${name} {
    %for param in field_members:
    ${get_type_name(param.type)} ${param.name};
    %endfor

    ${name}(\
    %for param in param_members:
    ${get_type_name(param.type)} _${param.name} ${comma(param, param_members)}
    %endfor

    %if len(count_pointer_pairs) > 0 and len(param_members) > 0:
    ,
    %endif

    %for pair in count_pointer_pairs:
    <%
      type_name = get_type_name(pair[1].type.pointer_type).replace("const", "")
      param_name = pair[0].name.replace("Count", "")
    %>
     const std::vector<${type_name}>& ${param_name} ${comma(pair, count_pointer_pairs)}
    %endfor
    ) {
        *this = {};

        %for member in param_members:
        ${member.name} = _${member.name};
        %endfor

        %for pair in count_pointer_pairs:
        <%
        param_name = pair[0].name.replace("Count", "")
        %>

        ${pair[0].name} = ${param_name}.size();
        ${pair[1].name} = ${param_name}.data();
        %endfor
    }

    ${name}() {
        memset(this, 0, sizeof(${name}));
        %if not stype == "":
        sType = ${stype};
        %endif
    }

    explicit ${name}(const ${real_name}& value) {
        *this = *reinterpret_cast<const ${name}*>(&value);
    }

    operator ${real_name}() {
        ${real_name} value;
        value = *reinterpret_cast<${real_name}*>(this);

        return value;
    }
};
