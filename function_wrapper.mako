<%def name="expand_param(param)">
%if param.type.array_size > 1:
  ${param.name + ".data()"}\
%elif is_wrapped_type(param.type):
  reinterpret_cast<${param.type.name}>(${param.name})\
%else:
  ${param.name}\
%endif
</%def>

<%def name="comma(item, items)">
%if not item == items[-1]:
  ,\
%endif
</%def>

%if function.return_type.name == "VkResult":
void \
%else:
${function.return_type.name} \
%endif

${function.name.replace("vk", "")} (\
%for param_info in params:

%if param_info["is_array_pointer_param"]:
<%
  type_name = get_type_name(param_info["array_pointer_param"].type.pointer_type).replace("const", "")
  param_name = param_info["array_param_name"]
%>
const std::vector<${type_name}>& ${param_name} ${comma(param_info, params)}
%elif not (param_info["is_array_count_param"] or param_info["is_array_pointer_param"]):
${get_type_name(param_info["param"].type)} ${param_info["param"].name} ${comma(param_info, params)}
%endif


%endfor
) {
%if not (function.return_type.name == "void" or function.return_type.name == "VkResult"):
    return
%endif:

%if function.return_type.name == "VkResult":
    CheckResult
%endif:
    (${function.name}(\
    %for param_info in params:
    %if param_info["is_array_pointer_param"]:
    reinterpret_cast<${param_info["param"].type.name}>(${param_info["array_param_name"]}.data())
    %elif param_info["is_array_count_param"]:
    ${param_info["array_param_name"]}.size()
    %else:
    ${expand_param(param_info["param"])}
    %endif
    ${comma(param_info, params)}\
    %endfor

    ));
}
