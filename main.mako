#pragma once
#include <array>
namespace vk {
    std::string ResultToString(VkResult result) {
  if (result == VK_SUCCESS)
      return "VK_SUCCESS";

  if (result == VK_NOT_READY)
      return "VK_NOT_READY";

  if (result == VK_TIMEOUT)
      return "VK_TIMEOUT";

  if (result == VK_EVENT_SET)
      return "VK_EVENT_SET";

  if (result == VK_EVENT_RESET)
      return "VK_EVENT_RESET";

  if (result == VK_INCOMPLETE)
      return "VK_INCOMPLETE";

  if (result == VK_ERROR_OUT_OF_HOST_MEMORY)
      return "VK_ERROR_OUT_OF_HOST_MEMORY";

  if (result == VK_ERROR_OUT_OF_DEVICE_MEMORY)
      return "VK_ERROR_OUT_OF_DEVICE_MEMORY";

  if (result == VK_ERROR_INITIALIZATION_FAILED)
      return "VK_ERROR_INITIALIZATION_FAILED";

  if (result == VK_ERROR_DEVICE_LOST)
      return "VK_ERROR_DEVICE_LOST";

  if (result == VK_ERROR_MEMORY_MAP_FAILED)
      return "VK_ERROR_MEMORY_MAP_FAILED";

  if (result == VK_ERROR_LAYER_NOT_PRESENT)
      return "VK_ERROR_LAYER_NOT_PRESENT";

  if (result == VK_ERROR_EXTENSION_NOT_PRESENT)
      return "VK_ERROR_EXTENSION_NOT_PRESENT";

  if (result == VK_ERROR_FEATURE_NOT_PRESENT)
      return "VK_ERROR_FEATURE_NOT_PRESENT";

  if (result == VK_ERROR_INCOMPATIBLE_DRIVER)
      return "VK_ERROR_INCOMPATIBLE_DRIVER";

  if (result == VK_ERROR_TOO_MANY_OBJECTS)
      return "VK_ERROR_TOO_MANY_OBJECTS";

  if (result == VK_ERROR_FORMAT_NOT_SUPPORTED)
      return "VK_ERROR_FORMAT_NOT_SUPPORTED";

  if (result == VK_ERROR_SURFACE_LOST_KHR)
      return "VK_ERROR_SURFACE_LOST_KHR";

  if (result == VK_ERROR_NATIVE_WINDOW_IN_USE_KHR)
      return "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR";

  if (result == VK_SUBOPTIMAL_KHR)
      return "VK_SUBOPTIMAL_KHR";

  if (result == VK_ERROR_OUT_OF_DATE_KHR)
      return "VK_ERROR_OUT_OF_DATE_KHR";

  if (result == VK_ERROR_INCOMPATIBLE_DISPLAY_KHR)
      return "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR";

  if (result == VK_ERROR_VALIDATION_FAILED_EXT)
      return "VK_ERROR_VALIDATION_FAILED_EXT";

  if (result == VK_ERROR_INVALID_SHADER_NV)
      return "VK_ERROR_INVALID_SHADER_NV";

  return "Unknown Result";
}

void CheckResult(VkResult result) {
  if (result != VkResult::VK_SUCCESS) {
      auto message = ResultToString(result);
      throw std::runtime_error(message);
  }
}

${structs}

${functions}

}
