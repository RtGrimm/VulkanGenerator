import clang.cindex

class Type:
    def __init__(self, name, array_size, array_element_type, is_struct, pointer_type, canonical_type, is_enum):
        self.name = name
        self.array_size = array_size
        self.array_element_type = array_element_type
        self.is_struct = is_struct
        self.pointer_type = pointer_type
        self.canonical_type = canonical_type
        self.is_enum = is_enum


def type_from_clang(type):
    if type.spelling == "":
        return None

    canonical_type = None

    if not type.get_canonical().spelling == type.spelling:
        canonical_type = type_from_clang(type.get_canonical())

    return Type(type.spelling,
                type.get_array_size(),
                type_from_clang(type.get_array_element_type()),
                type.kind == clang.cindex.TypeKind.RECORD,
                type_from_clang(type.get_pointee()),
                canonical_type,
                type.kind == clang.cindex.TypeKind.ENUM)


class Field:
    def __init__(self, name, type):
        self.name = name
        self.type = type


class Struct:
    def __init__(self, name, members):
        self.name = name
        self.members = members


class Parameter:
    def __init__(self, name, type):
        self.name = name
        self.type = type


class Function:
    def __init__(self, name, return_type, parameters):
        self.name = name
        self.parameters = parameters
        self.return_type = return_type


class EnumConstant:
    def __init__(self, name, value):
        self.name = name
        self.value = value


class Enum:
    def __init__(self, name, constants):
        self.name = name
        self.constants = constants


class TranslationUnit:
    def __init__(self, structs, enums, functions):
        self.structs = structs
        self.enums = enums
        self.functions = functions


def filter_by_kind(cursor, kind):
    return filter(lambda field_cursor:
                  field_cursor.kind == kind,
                  cursor.get_children())


def parse_functions(cursor):
    def get_parameters(function_cursor):
        parameters = filter_by_kind(
            function_cursor, clang.cindex.CursorKind.PARM_DECL)

        return [Parameter(param.spelling, type_from_clang(param.type))
                for param in parameters]

    functions = filter_by_kind(cursor, clang.cindex.CursorKind.FUNCTION_DECL)

    return [Function(function.spelling,
                     type_from_clang(function.result_type),
                     get_parameters(function))
            for function in functions]


def parse_structs(cursor):
    def get_fields(struct_cursor):
        fields = filter_by_kind(
            struct_cursor,
            clang.cindex.CursorKind.FIELD_DECL)

        return map(
            lambda field: Field(field.spelling,
                                type_from_clang(field.type)), fields)

    structs = filter_by_kind(
        cursor,
        clang.cindex.CursorKind.STRUCT_DECL)

    return map(
        lambda struct: Struct(struct.spelling, get_fields(struct)), structs)


def parse_enums(cursor):
    def get_enum_constants(struct_cursor):
        constants = filter_by_kind(
            struct_cursor,
            clang.cindex.CursorKind.ENUM_CONSTANT_DECL)

        return map(
            lambda constant: EnumConstant(constant.spelling, constant.enum_value), constants)

    enums = filter_by_kind(
        cursor,
        clang.cindex.CursorKind.ENUM_DECL)

    return map(
        lambda enum: Enum(enum.spelling, get_enum_constants(enum)), enums)