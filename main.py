import clang.cindex
import re
from mako.template import Template
import clang_parse
import os


def get_type_name(type, structs):
    type_name = type.name

    if is_wrapped_type(structs, type):
        type_name = type_name.replace("Vk", "")

    if type.array_size > 1:
        type_name = "std::array<{0}, {1}>".format(
            type.array_element_type.name, str(type.array_size))

    return type_name


def is_wrapped_type(structs, type):
    is_pointer = type.pointer_type is not None

    if type.is_struct or (type.canonical_type is not None and type.canonical_type.is_struct):
        name = type.name
    elif is_pointer and type.pointer_type.canonical_type is not None and type.pointer_type.canonical_type.is_struct:
        name = type.pointer_type.name
    else:
        return False

    correct_name = name.replace("const", "").replace(" ", "")
    return any([struct.name == correct_name for struct in structs])


def get_array_count_pairs(members):
    def compare_count_pointer(count, pointer):
        return count.name.replace("Count", "").lower() == pointer.name.replace("ies", "y").strip("ps").lower()

    array_count_params = [member for member in members
                          if ("Count" in member.name) and member.type.name == "uint32_t" and
                          any(
                              compare_count_pointer(member, pointer_member)
                              for pointer_member in members)
                          ]

    array_params = [member for member in members if member.type.pointer_type is not None and
                    any(
                        compare_count_pointer(array_count_param, member)
                        for array_count_param in array_count_params)
                    ]

    return (array_count_params, array_params)


def load_template(path):
    file = open(path, "r")
    data = file.read()
    file.close()

    template = Template(data)

    return template


def generate_struct_wrappers(translation_unit):
    structure_type_enum = filter(
        lambda enum: enum.name == "VkStructureType", translation_unit.enums)[0]

    output = ""

    template = load_template("struct_wrapper.mako")

    wrapped_structs = [struct for struct in
                       translation_unit.structs if len(struct.members) > 0]

    for struct in wrapped_structs:

        (array_count_params, array_members) = get_array_count_pairs(struct.members)

        count_pointer_pairs = zip(array_count_params, array_members)

        param_members = [member for member in struct.members
                         if (not (member.name == "pNext" or member.name == "sType")) and
                         not (member in array_count_params or member in array_members)]

        sType = ""

        if any([field.name == "sType" for field in struct.members]):
            sType = [constant for constant in structure_type_enum.constants
                     if constant.name
                         .replace("STRUCTURE_TYPE", "")
                         .replace("_", "").lower() == struct.name.lower()][0].name

        output += template.render(
            real_name=struct.name,
            name=struct.name.replace("Vk", ""),
            field_members=struct.members,
            stype=sType,
            param_members=param_members,
            count_pointer_pairs=count_pointer_pairs,
            get_type_name=lambda type: get_type_name(type, wrapped_structs)
        )

    return output


def generate_functions(translation_unit):
    output = ""

    template = load_template("./function_wrapper.mako")

    wrapped_structs = [struct for struct in
                       translation_unit.structs if len(struct.members) > 0]

    for function in translation_unit.functions:
        if "EXT" in function.name:
            continue


        (array_count_params, array_members) = get_array_count_pairs(function.parameters)
        array_count_pairs =  zip(array_count_params, array_members)

        params = []

        array_param_offset = 0
        array_count_offset = 0

        def name_from_array_count_param(param):
            return param.name.replace("Count", "") + "s"

        for param_info in function.parameters:
            if param_info in array_members:
                params.append({
                    "is_array_pointer_param": True,
                    "is_array_count_param": False,
                    "array_count_param": array_count_pairs[array_param_offset][0],
                    "array_pointer_param": array_count_pairs[array_param_offset][1],
                    "array_param_name": name_from_array_count_param(array_count_pairs[array_param_offset][0]),
                    "param": param_info
                })

                array_param_offset += 1

            elif param_info in array_count_params:
                params.append({
                    "is_array_pointer_param" : False,
                    "is_array_count_param": True,
                    "array_count_param": array_count_pairs[array_param_offset][0],
                    "array_pointer_param": array_count_pairs[array_param_offset][1],
                    "array_param_name": name_from_array_count_param(array_count_pairs[array_param_offset][0]),
                    "param": param_info
                })

                array_count_offset += 1
            else:
                params.append({
                    "is_array_pointer_param" : False,
                    "is_array_count_param": False,
                    "param": param_info
                })



        output += template.render(
            function_name=function.name.replace("vk", ""),
            real_function_name=function.name,
            function=function,
            params=params,
            get_type_name=lambda type: get_type_name(type, wrapped_structs),
            is_wrapped_type=lambda type: is_wrapped_type(wrapped_structs, type),
            count_pointer_pairs=zip(array_count_params, array_members)
        )

    return output


def main():
    clang.cindex.Config.set_library_file("/usr/lib/libclang.so")

    includePaths = ["-I{0}".format(path) for path in ["/usr/include/c++/5",
                                                      "/usr/include/x86_64-linux-gnu/c++/5",
                                                      "/usr/include/c++/5/backward",
                                                      "/usr/lib/gcc/x86_64-linux-gnu/5/include",
                                                      "/usr/local/include",
                                                      "/usr/lib/gcc/x86_64-linux-gnu/5/include-fixed",
                                                      "/usr/include/x86_64-linux-gnu",
                                                      "/usr/include"]]

    main_template = load_template("main.mako")

    index = clang.cindex.Index.create()
    unit = index.parse("./vulkan/vulkan.h", includePaths)

    structs = clang_parse.parse_structs(unit.cursor)
    functions = clang_parse.parse_functions(unit.cursor)
    enums = clang_parse.parse_enums(unit.cursor)

    translation_unit = clang_parse.TranslationUnit(structs, enums, functions)

    output = main_template.render(
        structs=generate_struct_wrappers(translation_unit),
        functions=generate_functions(translation_unit)
    )

    file = open("./vkInit.hpp", "w")
    file.write(output)
    file.close()


main()
